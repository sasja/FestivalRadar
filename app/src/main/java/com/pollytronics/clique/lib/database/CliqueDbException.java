package com.pollytronics.clique.lib.database;

/**
 * Created by pollywog on 7/1/15.
 */
public class CliqueDbException extends Exception {
    public CliqueDbException() { super(); }
    public CliqueDbException(String detailMessage) { super(detailMessage); }
}
